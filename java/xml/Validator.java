/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xml;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.RegularExpression;
import java.io.FileInputStream;
import java.io.InputStream;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

/**
 *
 * @author damjan
 */
public class Validator {
    public static void main(String[] args) throws Exception {
        validateAgainstXSD(new FileInputStream(args[0]), new FileInputStream(args[1]));
    }
    
    static void validateAgainstXSD(InputStream xsd, InputStream xml) throws Exception {
        SchemaFactory factory = 
            SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(new StreamSource(xsd));
        javax.xml.validation.Validator validator = schema.newValidator();
        validator.validate(new StreamSource(xml));
        
//        RegularExpression e = new RegularExpression("\\$");
//        e.setPattern("\\$a", "X");
//        System.out.println(e.matches("$"));
    }
}
