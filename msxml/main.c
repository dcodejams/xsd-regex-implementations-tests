#define COBJMACROS
#include <initguid.h>

#include "stdio.h"

#include "windows.h"


#include "msxml.h"
#include "msxml2.h"
#include "msxml2did.h"
#include "ole2.h"
#include "dispex.h"
#include "objsafe.h"
#include "mshtml.h"
#include "docobj.h"
#include "initguid.h"
#include "asptlb.h"
#include "shlwapi.h"


DEFINE_GUID(IID_IXMLDOMSchemaCollection, 0x373984c8, 0xb845, 0x449b, 0x91,0xe7, 0x45,0xac,0x83,0x03,0x6a,0xde);

//DEFINE_GUID(CLSID_XMLSchemaCache, 0x373984c9, 0xb845, 0x449b, 0x91,0xe7, 0x45,0xac,0x83,0x03,0x6a,0xde);
//DEFINE_GUID(CLSID_XMLSchemaCache30, 0xf5078f34, 0xc551, 0x11d3, 0x89,0xb9, 0x00,0x00,0xf8,0x1f,0xe2,0x21);
//DEFINE_GUID(CLSID_XMLSchemaCache40, 0x88d969c2, 0xf192, 0x11d4, 0xa6,0x5f, 0x00,0x40,0x96,0x32,0x51,0xe5);
DEFINE_GUID(CLSID_XMLSchemaCache60, 0x88d96a07, 0xf192, 0x11d4, 0xa6,0x5f, 0x00,0x40,0x96,0x32,0x51,0xe5);

DEFINE_GUID(IID_IXMLDOMDocument2, 0x2933bf95, 0x7b36, 0x11d2, 0xb2,0x0e, 0x00,0xc0,0x4f,0x98,0x3e,0x60);

//DEFINE_GUID(CLSID_DOMDocument26, 0xf5078f1b, 0xc551, 0x11d3, 0x89,0xb9, 0x00,0x00,0xf8,0x1f,0xe2,0x21);
//DEFINE_GUID(CLSID_DOMDocument30, 0xf5078f32, 0xc551, 0x11d3, 0x89,0xb9, 0x00,0x00,0xf8,0x1f,0xe2,0x21);
//DEFINE_GUID(CLSID_DOMDocument40, 0x88d969c0, 0xf192, 0x11d4, 0xa6,0x5f, 0x00,0x40,0x96,0x32,0x51,0xe5);
DEFINE_GUID(CLSID_DOMDocument60, 0x88d96a05, 0xf192, 0x11d4, 0xa6,0x5f, 0x00,0x40,0x96,0x32,0x51,0xe5);

int main(int argc, char **argv)
{
    IXMLDOMSchemaCollection *schemaCollection = NULL;
    IXMLDOMDocument2 *document = NULL;
    IXMLDOMParseError* err;
    VARIANT v;
    VARIANT_BOOL b;
    WCHAR wbuffer[MAX_PATH];
    HRESULT hr;

    CoInitialize(NULL);

    hr = CoCreateInstance(&CLSID_XMLSchemaCache60, NULL, CLSCTX_INPROC_SERVER, &IID_IXMLDOMSchemaCollection, (void**)&schemaCollection);
    if (FAILED(hr)) {
        printf("Error creating schema collection\n");
        return 1;
    }

    hr = CoCreateInstance(&CLSID_DOMDocument60, NULL, CLSCTX_INPROC_SERVER, &IID_IXMLDOMDocument2, (void**)&document);
    if (FAILED(hr)) {
        printf("Error creating document\n");
        return 1;
    }

    VariantInit(&v);
    V_VT(&v) = VT_BSTR;
    MultiByteToWideChar(CP_ACP, 0, argv[1], -1, wbuffer, sizeof(wbuffer)/sizeof(wbuffer[0]));
    V_BSTR(&v) = SysAllocString(wbuffer);
    hr = IXMLDOMSchemaCollection_add(schemaCollection, SysAllocString(L"") /*SysAllocString(L"https://www.w3schools.com")*/, v);
    if (FAILED(hr)) {
        printf("Adding schema failed, hr = 0x%08x\n", hr);
        return 1;
    }

    VariantInit(&v);
    V_VT(&v) = VT_UNKNOWN;
    V_UNKNOWN(&v) = (IUnknown*)schemaCollection;
    hr = IXMLDOMDocument2_putref_schemas(document, v);
    if (FAILED(hr)) {
        printf("Error putting schemas\n");
        return 1;
    }

    MultiByteToWideChar(CP_ACP, 0, argv[2], -1, wbuffer, sizeof(wbuffer)/sizeof(wbuffer[0]));
    VariantInit(&v);
    V_VT(&v) = VT_BSTR;
    V_BSTR(&v) = SysAllocString(wbuffer);
    hr = IXMLDOMDocument2_load(document, v, &b);
    if (FAILED(hr)) {
        printf("Error loading\n");
        return 1;
    }

    err = NULL;
    hr = IXMLDOMDocument2_validate(document, &err);
    printf("Valid hr = 0x%08x\n", hr);

    return 0;
}

