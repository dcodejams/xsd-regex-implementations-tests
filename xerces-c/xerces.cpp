#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <stdlib.h>
#include <string.h>
#if defined(XERCES_NEW_IOSTREAMS)
#include <iostream>
#else
#include <iostream.h>
#endif
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/ErrorHandler.hpp>
#include <xercesc/sax/SAXParseException.hpp>

using namespace xercesc_3_2;
using namespace std;

 
class ParserErrorHandler : public ErrorHandler
{
    private:
        void reportParseException(const SAXParseException& ex)
        {
            char* msg = XMLString::transcode(ex.getMessage());
            fprintf(stderr, "at line %llu column %llu, %s\n", 
                    ex.getColumnNumber(), ex.getLineNumber(), msg);
            XMLString::release(&msg);
        }
 
    public:
        void warning(const SAXParseException& ex)
        {
            reportParseException(ex);
        }
 
        void error(const SAXParseException& ex)
        {
            reportParseException(ex);
        }
 
        void fatalError(const SAXParseException& ex)
        {
            reportParseException(ex);
        }
 
        void resetErrors()
        {
        }
};

int main(int argc, char **argv)
{
    cout << "Starting" << endl;
    XMLPlatformUtils::Initialize();

    // Instantiate the DOM parser.
    XercesDOMParser parser;
    ParserErrorHandler parserErrorHandler;
    parser.setErrorHandler(&parserErrorHandler);
    parser.setDoNamespaces( true );
    parser.setDoSchema( true );
    parser.setLoadSchema( true );
    parser.setGenerateSyntheticAnnotations( true );
    parser.setValidationScheme(XercesDOMParser::Val_Always);
    parser.setValidationSchemaFullChecking( true );
    parser.setValidateAnnotations( true );
    parser.setValidationConstraintFatal(true);
    parser.setCalculateSrcOfs( true );
    parser.cacheGrammarFromParse( true );
    parser.useCachedGrammarInParse( true );
    parser.setCreateSchemaInfo( true );
    parser.setCreateCommentNodes( true );

    if (parser.loadGrammar(argv[1], Grammar::SchemaGrammarType) == NULL) {
        cout << "couldn't load grammar" << endl;
        return 1;
    }

//    char location[1024];
//    snprintf(location, sizeof(location)/sizeof(location[0]), "urn:test %s", argv[1]);
//    parser.setExternalSchemaLocation(location);
//    parser.setExternalNoNamespaceSchemaLocation(argv[1]);
    cout << "Parser ready" << endl;

    parser.parse(argv[2]);
    cout << "errors: " << parser.getErrorCount() << endl;


    return parser.getErrorCount() > 0 ? 1 : 0;
}
