using System.Xml;
using System.Xml.Schema;
using System.IO;
using System;

namespace dotnet
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(args[1]);

            xml.Schemas.Add(null, args[0]);

            xml.Validate(null);
        }
    }
}
