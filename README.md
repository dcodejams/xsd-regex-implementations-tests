# xsd-regex-implementations-tests

A collection of tests for different implementations' support
for non-standard regular expression escape sequences in the
XSD "pattern" element.

It is currently a very manual setup :(.

## Dependencies

- Linux or FreeBSD
- Java >= 1.8
- .NET Core 3.1
- Apache Xerces-C (optional, currently broken)
- mingw-w32 (optional)

## Building

- Java: cd to java/ and run `javac xml/Validator.java`
- .NET: cd to dotnet/ and run `dotnet build`
- Apache Xerces-C: cd to xerces-c and run `scons`
- MSXML: cd to msxml/ and run `i686-w64-mingw32-gcc main.c -o main.exe -lole32 -loleaut32`

## Running

Each tool takes the XSD file as its first parameter, and the XML file as its second parameter.

The `test.sh` Bash script runs the Java and .NET tests. It outputs the results in
CSV format that you should be able to copy and paste into a spreadsheet.

Apache Xerces-C is hopelessly broken and hours of trying couldn't get it to work.

MSXML has to be tested manually. Copy `main.exe` and `xml/` to a Windows system, and
for each xml/xsd file pair under `xml\`, run `main.exe xml\file.xsd xml\file.xml`.
A HRESULT of 00000000 is success, 00000001 is failure to match the schema,
and anything else is an earlier error (loading files, creating COM objects, etc.).
