#!/bin/bash

for letter in \
    at \
    backquote \
    colon \
    comma \
    dollar \
    doublequote \
    equals \
    exclamation \
    greaterthan \
    hash \
    percentage \
    semicolon \
    slash \
    tilde \
    u-surrogate-pair
do
    if java -cp java xml.Validator xml/${letter}.xsd xml/${letter}.xml &> /dev/null; then
        echo "\"Java\",\"${letter}\",\"Y\""
    else
        echo "\"Java\",\"${letter}\",\"N\""
    fi

    if dotnet/bin/Debug/netcoreapp3.1/dotnet xml/${letter}.xsd xml/${letter}.xml &> /dev/null; then
        echo "\".NET 3.1\",\"${letter}\",\"Y\""
    else
        echo "\".NET 3.1\",\"${letter}\",\"N\""
    fi

# Broken and hopeless
#    if xerces-c/xerces_test xml/${letter}.xsd xml/${letter}.xml &> /dev/null; then
#        echo "\"Apache Xerces-C\",\"${letter}\",\"Y\""
#    else
#        echo "\"Apache Xerces-C\",\"${letter}\",\"N\""
#    fi

done
